import React from "react";

const FunctionalComponent = ({ name, age }) => {
  return (
    <div>
      <h1>Komponen ini dibuat dengan functional component</h1>
      <h2>Selamat datang {name} </h2>
      <p>Umur: {age}</p>
    </div>
  );
};

export default FunctionalComponent;
