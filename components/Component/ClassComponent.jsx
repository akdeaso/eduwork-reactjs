import React from "react";

class ClassComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
    };
  }

  decreaseCount = () => {
    if (this.state.count > 0) {
      this.setState({ count: this.state.count - 1 });
    }
  };

  increaseCount = () => {
    this.setState({ count: this.state.count + 1 });
  };

  render() {
    return (
      <div>
        <h1>Komponen ini dibuat dengan class component</h1>
        <p>Count: {this.state.count}</p>
        <button onClick={this.decreaseCount}>-</button>
        <span> </span>
        <button onClick={this.increaseCount}>+</button>
      </div>
    );
  }
}

export default ClassComponent;
