import React from "react";
import ClassComponent from "./Component/ClassComponent";
import FunctionalComponent from "./Component/FunctionalComponent";

const Component = () => {
  return (
    <>
      <ClassComponent />
      <FunctionalComponent name={"Tabula Smaragdina"} age={25} />
    </>
  );
};

export default Component;
